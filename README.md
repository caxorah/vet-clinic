There's a Vet clinic that struggles with online appointment registration. Clinic's website allows to send appointments requests via email. Unfortunately customers often do not send complete set of informations about the appointment and employees has to reach such customers to get more details. As described, it is manual process and employees and doctors must spend their time fitting appointments into their calendars.

###Implement an application that exposes RESTful API that:
- allows customer to make an appointment with certain doctor at certain date and time
- allows to list appointments of certain doctor for given day
- allows customer to cancel his appointment

###NOTE: For sake of ease of implementation:
- Please skip security aspects, neither authentication nor authorization is required.
- Lets assume that customers already exists in the application
- Lets assume that customers have to provide their 4 digit ID and 4 digit PIN in order to make or cancel their appointment

Design and technology is up to you. When you are finished with this task, please publish your code to publicly available GitHub repository and send me an email with information that it is ready to be reviewed.

Please also make sure that repository that you deliver:
- contains a master branch from which code could be deployed to production(no ci/cd deployment scripts or pipelines are needed)
- does not contain dead/unused code
- does not contain unnecessary files
- contains runnable code
