package com.example.vetclinic.persistence;

import com.example.vetclinic.persistence.appointments.AppointmentEntity;
import com.example.vetclinic.persistence.appointments.AppointmentRepositoryJpa;
import com.example.vetclinic.persistence.customers.CustomerEntity;
import com.example.vetclinic.persistence.customers.CustomerRepositoryJpa;
import com.example.vetclinic.persistence.doctors.DoctorEntity;
import com.example.vetclinic.persistence.doctors.DoctorRepositoryJpa;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class AppointmentRepositoryTest {

    @Autowired
    AppointmentRepositoryJpa appointmentRepository;

    @Autowired
    DoctorRepositoryJpa doctorRepository;

    @Autowired
    CustomerRepositoryJpa customerRepository;

    private CustomerEntity testCustomer;
    private DoctorEntity testDoctor;
    private AppointmentEntity testAppointment;
    private static final LocalDate TEST_DATE = LocalDate.of(2022,2,25);

    @BeforeEach
    void prepareRecords() {
        CustomerEntity customer = new CustomerEntity();
        customer.setPin(1234L);
        customer.setName("customer 1");
        testCustomer = customerRepository.save(customer);

        DoctorEntity doctor = new DoctorEntity();
        doctor.setName("Doctor Proctor");
        testDoctor = doctorRepository.save(doctor);

        testAppointment = AppointmentEntity.builder()
                .date(TEST_DATE)
                .time(LocalTime.now())
                .customer(testCustomer)
                .doctor(testDoctor)
                .build();
    }


    @Test
    void should_save_new_appointment() {
        appointmentRepository.save(testAppointment);
        assertNotNull(appointmentRepository.findAll());
    }

    @Test
    void should_find_all_appointments_of_doctor_on_specific_date() {
        AppointmentEntity firstAppointment = AppointmentEntity.builder()
                .customer(testCustomer)
                .doctor(testDoctor)
                .date(TEST_DATE)
                .time(LocalTime.of(12,30))
                .build();

        AppointmentEntity secondAppointment = AppointmentEntity.builder()
                .customer(testCustomer)
                .doctor(testDoctor)
                .date(TEST_DATE)
                .time(LocalTime.of(14,0))
                .build();

        appointmentRepository.save(firstAppointment);
        appointmentRepository.save(secondAppointment);

        List<AppointmentEntity> appointments = appointmentRepository.findByDateAndDoctorId(TEST_DATE, testDoctor.getId());
        assertEquals(2, appointments.size());
    }

    @Test
    void should_delete_appointment_by_id() {
        Long id = appointmentRepository.save(testAppointment).getId();
        appointmentRepository.deleteById(id);
        Optional<AppointmentEntity> appointment = appointmentRepository.findById(id);
        assertTrue(appointment.isEmpty());
    }
}