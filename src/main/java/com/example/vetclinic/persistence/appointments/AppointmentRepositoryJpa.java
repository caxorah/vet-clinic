package com.example.vetclinic.persistence.appointments;

import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface AppointmentRepositoryJpa extends JpaRepository<AppointmentEntity, Long> {

    List<AppointmentEntity> findByDateAndDoctorId (LocalDate date, Long doctorId);
}
