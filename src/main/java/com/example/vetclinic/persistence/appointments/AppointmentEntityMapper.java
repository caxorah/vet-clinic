package com.example.vetclinic.persistence.appointments;

import com.example.vetclinic.domain.appointments.Appointment;
import com.example.vetclinic.persistence.customers.CustomerEntityMapper;
import com.example.vetclinic.persistence.doctors.DoctorEntityMapper;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {DoctorEntityMapper.class, CustomerEntityMapper.class})
public interface AppointmentEntityMapper {

    AppointmentEntity toEntity(Appointment appointment);

    Appointment toDomain(AppointmentEntity appointmentEntity);

    List<Appointment> toAppointmentsList(List<AppointmentEntity> appointmentEntityList);

}
