package com.example.vetclinic.persistence.appointments;

import com.example.vetclinic.domain.appointments.Appointment;
import com.example.vetclinic.domain.appointments.AppointmentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
@RequiredArgsConstructor
public class AppointmentRepositoryAdapter implements AppointmentRepository {

    private final AppointmentRepositoryJpa repository;
    private final AppointmentEntityMapper mapper;

    @Override
    public Appointment save(Appointment appointment) {
        AppointmentEntity entity = repository.save(mapper.toEntity(appointment));
        return mapper.toDomain(entity);
    }

    @Override
    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    @Override
    public List<Appointment> findByDateAndDoctorId(LocalDate date, Long doctorId) {
        List<AppointmentEntity> appointments = repository.findByDateAndDoctorId(date, doctorId);
        return mapper.toAppointmentsList(appointments);
    }
}
