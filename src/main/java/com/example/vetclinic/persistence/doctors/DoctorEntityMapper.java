package com.example.vetclinic.persistence.doctors;

import com.example.vetclinic.domain.doctors.Doctor;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface DoctorEntityMapper {

    DoctorEntity toEntity(Doctor doctor);

    Doctor toDomain(DoctorEntity doctorEntity);

}
