package com.example.vetclinic.persistence.doctors;

import com.example.vetclinic.domain.doctors.Doctor;
import com.example.vetclinic.domain.doctors.DoctorRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class DoctorRepositoryAdapter implements DoctorRepository {

    private final DoctorRepositoryJpa repository;
    private final DoctorEntityMapper mapper;

    @Override
    public Optional<Doctor> getById(Long doctorId) {
        Optional<DoctorEntity> doctor = repository.findById(doctorId);
        if (doctor.isEmpty()){
            return Optional.empty();
        }
        return Optional.of(mapper.toDomain(doctor.get()));
    }
}
