package com.example.vetclinic.persistence.doctors;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DoctorRepositoryJpa extends JpaRepository<DoctorEntity, Long> {

}
