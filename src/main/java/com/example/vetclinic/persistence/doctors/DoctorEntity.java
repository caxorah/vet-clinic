package com.example.vetclinic.persistence.doctors;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "doctors")
@Data
public class DoctorEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;

}
