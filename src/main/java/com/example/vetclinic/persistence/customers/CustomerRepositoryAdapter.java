package com.example.vetclinic.persistence.customers;

import com.example.vetclinic.domain.customers.Customer;
import com.example.vetclinic.domain.customers.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class CustomerRepositoryAdapter implements CustomerRepository {

    private final CustomerRepositoryJpa repository;
    private final CustomerEntityMapper mapper;

    @Override
    public Optional<Customer> getById(Long id) {
        Optional<CustomerEntity> customer = repository.findById(id);
        if (customer.isEmpty()){
            return Optional.empty();
        }
        return Optional.of(mapper.toDomain(customer.get()));
    }
}
