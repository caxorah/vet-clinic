package com.example.vetclinic.persistence.customers;

import lombok.Data;

import javax.persistence.*;


@Entity
@Table(name = "customers")
@Data
public class CustomerEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Long pin;
    private String name;

}
