package com.example.vetclinic.persistence.customers;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepositoryJpa extends JpaRepository<CustomerEntity, Long> {
}
