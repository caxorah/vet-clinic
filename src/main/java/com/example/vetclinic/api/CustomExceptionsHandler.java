package com.example.vetclinic.api;

import com.example.vetclinic.domain.commons.ResourceNotFoundException;
import com.example.vetclinic.domain.customers.CustomerAuthorizationFailureException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class CustomExceptionsHandler {

    @ExceptionHandler(CustomerAuthorizationFailureException.class)
    public ResponseEntity<String> onCustomerAuthorizationFailed(CustomerAuthorizationFailureException exception){
        return ResponseEntity
                .status(HttpStatus.FORBIDDEN)
                .body(exception.getMessage());
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<String> onResourceNotFound(ResourceNotFoundException exception) {
        return ResponseEntity
                .status(HttpStatus.NOT_FOUND)
                .body(exception.getMessage());
    }
}
