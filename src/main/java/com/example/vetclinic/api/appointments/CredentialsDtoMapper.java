package com.example.vetclinic.api.appointments;

import com.example.vetclinic.domain.customers.Credentials;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CredentialsDtoMapper {

    Credentials toDomain(CredentialsDto dto);
}
