package com.example.vetclinic.api.appointments;

import lombok.Value;

import java.time.LocalDate;
import java.time.LocalTime;

@Value
public class AppointmentRequestDto {

    LocalDate date;
    LocalTime time;
    Long doctorId;
    CredentialsDto credentialsDto;

}
