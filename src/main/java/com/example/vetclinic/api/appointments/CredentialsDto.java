package com.example.vetclinic.api.appointments;

import lombok.Value;

@Value
public class CredentialsDto {

    Long id;
    Long pin;

}
