package com.example.vetclinic.api.appointments;

import com.example.vetclinic.domain.appointments.Appointment;
import com.example.vetclinic.domain.appointments.AppointmentService;
import com.example.vetclinic.domain.customers.Credentials;
import com.example.vetclinic.domain.customers.CredentialsService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;


@RestController
@RequestMapping(path = "/appointments")
@RequiredArgsConstructor
public class AppointmentController {

    private final AppointmentService appointmentService;
    private final CredentialsService credentialsService;
    private final AppointmentDtoMapper appointmentMapper;
    private final AppointmentRequestDtoMapper appointmentRequestMapper;
    private final CredentialsDtoMapper credentialsMapper;

    @PostMapping
    public ResponseEntity<AppointmentDto> addAppointment(@RequestBody AppointmentRequestDto requestDto) {
        Credentials credentials = credentialsMapper.toDomain(requestDto.getCredentialsDto());
        credentialsService.authorize(credentials);
        Appointment appointment = appointmentService.saveAppointment(
                appointmentRequestMapper.toRequest(requestDto),
                credentials.getId());
        return ResponseEntity.ok(appointmentMapper.toDto(appointment));
    }

    @DeleteMapping("/{id}")
    public void cancelAppointment(@PathVariable Long id, @RequestBody Credentials credentials) {
        credentialsService.authorize(credentials);
        appointmentService.cancelAppointment(id);
    }

    @GetMapping
    public List<AppointmentDto> findDoctorsAppointmentsByDate(
            @RequestParam Long doctorId,
            @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date
    ) {
        List<Appointment> appointments = appointmentService.findBy(doctorId, date);
        return appointmentMapper.toDtoAppointments(appointments);
    }

}
