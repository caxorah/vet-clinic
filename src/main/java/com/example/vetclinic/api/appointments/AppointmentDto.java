package com.example.vetclinic.api.appointments;

import lombok.Builder;
import lombok.Value;

import java.time.LocalDate;
import java.time.LocalTime;

@Value
@Builder
public class AppointmentDto {

    Long id;
    LocalDate date;
    LocalTime time;
    Long doctorId;
    Long customerId;

}
