package com.example.vetclinic.api.appointments;

import com.example.vetclinic.domain.appointments.Appointment;
import com.example.vetclinic.domain.customers.Customer;
import com.example.vetclinic.domain.customers.CustomerService;
import com.example.vetclinic.domain.doctors.Doctor;
import com.example.vetclinic.domain.doctors.DoctorService;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Mapper(componentModel = "spring", uses = CredentialsDtoMapper.class)
public abstract class AppointmentDtoMapper {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private DoctorService doctorService;

    public AppointmentDto toDto(Appointment domain) {
        return AppointmentDto.builder()
                .id(domain.getId())
                .date(domain.getDate())
                .time(domain.getTime())
                .doctorId(domain.getDoctor().getId())
                .customerId(domain.getCustomer().getId())
                .build();
    }

    public Appointment toDomain(AppointmentDto dto) {
        Customer customer = customerService.getBy(dto.getCustomerId());
        Doctor doctor = doctorService.getBy(dto.getDoctorId());
        return Appointment.builder()
                .id(dto.getId())
                .date(dto.getDate())
                .time(dto.getTime())
                .customer(customer)
                .doctor(doctor)
                .build();
    }

    abstract List<AppointmentDto> toDtoAppointments (List<Appointment> appointments);

}
