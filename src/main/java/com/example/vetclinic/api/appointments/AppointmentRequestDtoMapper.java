package com.example.vetclinic.api.appointments;

import com.example.vetclinic.domain.appointments.AppointmentRequest;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface AppointmentRequestDtoMapper {

    AppointmentRequest toRequest(AppointmentRequestDto dtoRequest);

}
