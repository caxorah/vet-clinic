package com.example.vetclinic;

import com.example.vetclinic.domain.appointments.AppointmentRepository;
import com.example.vetclinic.domain.appointments.AppointmentService;
import com.example.vetclinic.domain.customers.CredentialsService;
import com.example.vetclinic.domain.customers.CustomerRepository;
import com.example.vetclinic.domain.customers.CustomerService;
import com.example.vetclinic.domain.doctors.DoctorRepository;
import com.example.vetclinic.domain.doctors.DoctorService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DomainConfiguration {

    @Bean
    public CustomerService customerService(CustomerRepository customerRepository) {
        return new CustomerService(customerRepository);
    }

    @Bean
    public DoctorService doctorService(DoctorRepository doctorRepository) {
        return new DoctorService(doctorRepository);
    }

    @Bean
    public AppointmentService appointmentService(
            DoctorService doctorService,
            CustomerService customerService,
            AppointmentRepository appointmentRepository
    ) {
      return new AppointmentService(doctorService, customerService, appointmentRepository);
    }

    @Bean
    public CredentialsService credentialsService(CustomerRepository customerRepository){
        return new CredentialsService(customerRepository);
    }

}
