package com.example.vetclinic.domain.doctors;

import com.example.vetclinic.domain.commons.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
public class DoctorService {

    private final DoctorRepository doctorRepository;

    public Doctor getBy(Long id) {
        Optional<Doctor> doctor = doctorRepository.getById(id);
        return doctor.orElseThrow(() -> new ResourceNotFoundException("Doctor not found"));
    }
}
