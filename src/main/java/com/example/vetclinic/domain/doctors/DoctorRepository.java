package com.example.vetclinic.domain.doctors;

import java.util.Optional;

public interface DoctorRepository {

    Optional<Doctor> getById(Long doctorId);
}
