package com.example.vetclinic.domain.doctors;

import lombok.Data;

@Data
public class Doctor {

    private Long id;
    private String name;

}
