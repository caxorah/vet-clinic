package com.example.vetclinic.domain.customers;

public class CustomerAuthorizationFailureException extends RuntimeException {

    public CustomerAuthorizationFailureException(String message) {
        super(message);
    }
}
