package com.example.vetclinic.domain.customers;

import com.example.vetclinic.domain.commons.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
public class CredentialsService {

    private final CustomerRepository customerRepository;

    public void authorize(Credentials credentials) {
        Optional<Customer> customer = customerRepository.getById(credentials.getId());
        if (customer.isEmpty()) {
            throw new ResourceNotFoundException("User " + credentials.getId() + " does not exist");
        }
        if (!customerPinCorrect(customer.get(), credentials.getPin())) {
            throw new CustomerAuthorizationFailureException("PIN incorrect!");
        }
    }

    private boolean customerPinCorrect(Customer customer, Long pin) {
        return customer.getPin().equals(pin);
    }
}
