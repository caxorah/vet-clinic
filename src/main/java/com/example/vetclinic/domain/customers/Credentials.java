package com.example.vetclinic.domain.customers;

import lombok.Value;

@Value
public class Credentials {

    Long id;
    Long pin;

}
