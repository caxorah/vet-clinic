package com.example.vetclinic.domain.customers;

import lombok.*;

@Data
@Builder
public class Customer {

    private Long id;
    private Long pin;
    private String name;

}
