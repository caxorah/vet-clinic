package com.example.vetclinic.domain.customers;

import java.util.Optional;

public interface CustomerRepository {

    Optional<Customer> getById(Long id);
}
