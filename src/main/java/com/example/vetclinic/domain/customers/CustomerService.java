package com.example.vetclinic.domain.customers;

import com.example.vetclinic.domain.commons.ResourceNotFoundException;
import lombok.RequiredArgsConstructor;

import java.util.Optional;

@RequiredArgsConstructor
public class CustomerService {

    private final CustomerRepository customerRepository;

    public Customer getBy(Long id) {
        Optional<Customer> customer = customerRepository.getById(id);
        return customer.orElseThrow(() -> new ResourceNotFoundException("Customer not found"));
    }

}
