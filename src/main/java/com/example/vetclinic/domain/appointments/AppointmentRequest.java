package com.example.vetclinic.domain.appointments;

import com.example.vetclinic.domain.customers.Credentials;
import lombok.Value;

import java.time.LocalDate;
import java.time.LocalTime;

@Value
public class AppointmentRequest {

    LocalDate date;
    LocalTime time;
    Long doctorId;
    Credentials credentials;

}
