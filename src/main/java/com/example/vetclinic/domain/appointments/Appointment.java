package com.example.vetclinic.domain.appointments;

import com.example.vetclinic.domain.customers.Customer;
import com.example.vetclinic.domain.doctors.Doctor;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalTime;

@Data
@Builder
public class Appointment {

    private Long id;
    private LocalDate date;
    private LocalTime time;
    private Doctor doctor;
    private Customer customer;

}
