package com.example.vetclinic.domain.appointments;

import java.time.LocalDate;
import java.util.List;

public interface AppointmentRepository {

    Appointment save(Appointment appointment);

    void deleteById(Long id);

    List<Appointment> findByDateAndDoctorId(LocalDate date, Long doctorId);
}
