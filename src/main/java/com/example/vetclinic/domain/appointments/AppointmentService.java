package com.example.vetclinic.domain.appointments;

import com.example.vetclinic.domain.customers.Customer;
import com.example.vetclinic.domain.customers.CustomerService;
import com.example.vetclinic.domain.doctors.Doctor;
import com.example.vetclinic.domain.doctors.DoctorService;
import lombok.RequiredArgsConstructor;

import java.time.LocalDate;
import java.util.List;


@RequiredArgsConstructor
public class AppointmentService {

    private final DoctorService doctorService;
    private final CustomerService customerService;
    private final AppointmentRepository appointmentRepository;

    public Appointment saveAppointment(AppointmentRequest request, Long customerId) {
        Appointment appointment = appointmentFromRequest(request, customerId);
        return appointmentRepository.save(appointment);
    }


    private Appointment appointmentFromRequest(AppointmentRequest request, Long customerId) {
        Doctor doctor = doctorService.getBy(request.getDoctorId());
        Customer customer = customerService.getBy(customerId);
        return Appointment.builder()
                .customer(customer)
                .doctor(doctor)
                .date(request.getDate())
                .time(request.getTime())
                .build();
    }

    public void cancelAppointment(Long id) {
        appointmentRepository.deleteById(id);
    }

    public List<Appointment> findBy(Long doctorId, LocalDate date) {
        return appointmentRepository.findByDateAndDoctorId(date,doctorId);
    }
}
